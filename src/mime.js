function mime(filename){
    if (filename.includes('.')) {
        const ext = filename.split('.').at(-1).toLowerCase();
        if (types[ext]) {
            return types[ext];
        } else {
            return "application/octet-stream";
        }
    }else{
        return "application/octet-stream";
    }
}

const types = {
    "png": 'image/png',
    "jpg": 'image/jpeg',
    "jpeg": 'image/jpeg',
    "txt": 'text/plain',
    "avi": 'video/x-msvideo',
    "bin": 'application/octet-stream',
    "bmp": 'image/bmp',
    "bz": 'application/x-bzip',
    "bz2": 'application/x-bzip2',
    "css": 'text/css',
    "csv": 'text/csv',
    "epub": 'application/epub+zip',
    "gz": 'application/gzip',
    "gif": 'image/gif',
    "html": 'text/html',
    "htm": 'text/html',
    "ico": 'image/vnd.microsoft.icon',
    "jar": 'application/java-archive',
    "js": 'text/javascript',
    "json": 'application/json',
    "mid": 'audio/midi',
    "midi": 'audio/midi',
    "gmi": 'text/gemini',
    "gemini": 'text/gemini',
    "mp3": 'audio/mpeg',
    "mp4": 'video/mp4',
    "mpeg": 'video/mpeg',
    "oga": 'audio/ogg',
    "ogg": 'audio/ogg',
    "ogv": 'video/ogg',
    "ogx": 'application/ogg',
    "otf": 'font/otf',
    "pdf": 'application/pdf',
    "opus": 'audio/opus',
    "rar": 'application/vnd.rar',
    "rtf": 'application/rtf',
    "sh": 'application/x-sh',
    "svg": 'image/svg+xml',
    "swf": 'application/x-shockwave-flash',
    "tar": 'application/x-tar',
    "ttf": 'font/ttf',
    "wav": 'audio/wav',
    "weba": 'audio/webm',
    "webm": 'video/webm',
    "webp": 'image/webp',
    "woff": 'font/woff',
    "woff2": 'font/woff2',
    "xhtml": 'application/xhtml+xml',
    "xml": 'application/xml',
    "zip": 'application/zip',
    "7z": 'application/x-7z-compressed'
}

module.exports = mime;
