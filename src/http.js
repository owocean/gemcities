const express = require('express');
const app = express();
const gemtext = require('gemtext');
const fs = require('fs');
const e = require('express');

app.get('/', (req,res) => {
    res.send(gem(fs.readFileSync('static/index.gmi').toString()));
});

app.get('/*', (req,res) => {
    if (fs.existsSync('static/gettingstarted.gmi')){
        res.send(gem(fs.readFileSync('static/gettingstarted.gmi').toString()));
    } else {
        res.send(gem("# Woah there!\n\nYou need to be on the gemini protocol to view this page.\n=> gemini://cities.yesterweb.org/"));
    }
});

function gem(text) {
    return `<style>
    @import url('https://fonts.googleapis.com/css2?family=Roboto+Mono&display=swap');
    body{margin:5%;margin-right:25%;margin-left:25%;font-family:'Roboto Mono',monospace;line-height:150%;background:#111111;color:white;}
    h1:before{content:"# ";}
    h1{transform:translate(-1.25em);float:left;width:125%;height:5%;}
    a::before{content:"=> ";text-decoration:none;}
    a{transform:translate(-1.95em);float:left;color:#0087BD;}
    </style>`+
    gemtext.parse(text).generate(gemtext.HTMLRenderer)
}

app.listen(8050);
console.log("HTTP listening on 8050")