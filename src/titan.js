const fs = require('fs');
const account = require('./account');
const p = require('path');

function fileManager(sock, query, path) {
    let fingerprint = sock.getPeerCertificate(true).fingerprint256;
    if (fingerprint) {
        let fingerprints = JSON.parse(fs.readFileSync('fingerprints.json').toString());
        if (fingerprints[fingerprint]) {
            let username = fingerprints[fingerprint];
            if(query){
                if(path.endsWith("~newfile")) {
                    let dir = p.join('sites/'+username+'/',query).replace(/\\/g,"/");
                    try {
                        fs.writeFileSync(decodeURIComponent(dir), "");
                        sock.write('30 '+p.join("/filemanager/",decodeURIComponent(query)).replace(/\\/g,'/')+"\r\n");
                    }catch(err){
                        sock.write('50 Impossible operation\r\n');
                    }
                } else if(path.endsWith("~newdirectory")) {
                    let dir = p.join('sites/'+username+'/',query).replace(/\\/g,"/");
                    try {
                        fs.mkdirSync(dir);
                        sock.write("30 /filemanager/\r\n");
                    }catch(err){
                        sock.write('50 Impossible operation\r\n');
                    }
                } else if(path.endsWith("~movefile")) {
                    let dir = p.join('sites/'+username+'/',path.replace('/filemanager',"")).replace(/\\/g,"/").replace('~movefile',"");
                    if (!fs.existsSync(dir)) return sock.write('30 /filemanager\r\n');
                    let newDir = decodeURIComponent(p.join('sites/'+username+'/',query).replace(/\\/g,"/"));
                    if (newDir.includes('//')) return sock.write('30 /filemanager\r\n');
                    try {
                        fs.renameSync(dir, newDir);
                        sock.write('30 '+p.join("/filemanager/",decodeURIComponent(query)).replace(/\\/g,'/')+"\r\n");
                    }catch(err){
                        sock.write('50 Impossible operation\r\n');
                    }
                } else if(path.endsWith("~delfile")){
                    if (query.toLowerCase() == "y" || query.toLowerCase() == "yes"){
                        let dir = p.join('sites/'+username+'/',path.replace('/filemanager',"")).replace(/\\/g,"/").replace('~delfile',"");
                        if (!fs.existsSync(dir)) return sock.write('30 /filemanager\r\n');
                        fs.rmSync(dir);
                        sock.write('30 /filemanager/\r\n');
                    }
                } else if(path.endsWith("~deldirectory")){
                    if (query.toLowerCase() == "y" || query.toLowerCase() == "yes"){
                        let dir = p.join('sites/'+username+'/',path.replace('/filemanager',"")).replace(/\\/g,"/").replace('~deldirectory',"");
                        if (!fs.existsSync(dir)) return sock.write('30 /filemanager\r\n');
                        fs.rmSync(dir,{recursive:true});
                        sock.write('30 /filemanager/\r\n');
                    }
                } else if(query == "raw") {
                    let filepath = path.replace('/filemanager', "");
                    showFiles(username, filepath, sock, true);
                }
            }else{
                if (path.endsWith("~newfile")){
                    sock.write('10 Enter your filename (full path):\r\n');
                }else if (path.endsWith("~newdirectory")){
                    sock.write('10 Enter your directory name (full path):\r\n');
                }else if (path.endsWith("~movefile")){
                    sock.write('10 Enter the new file path (full path):\r\n');
                }else if (path.endsWith("~delfile")){
                    sock.write('10 Are you SURE you want to delete this file? [N]o/[Y]es:\r\n');
                }else if (path.endsWith("~deldirectory")){
                    sock.write('10 Are you SURE you want to delete this directory? [N]o/[Y]es:\r\n');
                }else{
                    let filepath = path.replace('/filemanager', "");
                    showFiles(username, filepath, sock);
                }
            }
        } else {
            if (path.startsWith('/filemanager/')) {
                if (path == '/filemanager/username') {
                    if (query) {
                        sock.write('30 /filemanager/password/' + query + '\r\n');
                    } else {
                        sock.write('10 This certificate is not linked to an account. Please enter your username\r\n');
                    }
                } else if (path.startsWith('/filemanager/password/')) {
                    if (query) {
                        let username = path.split('/')[3];
                        let password = decodeURIComponent(query);
                        if (account.login(username, password)) {
                            fingerprints[fingerprint] = username;
                            fs.writeFileSync('fingerprints.json', JSON.stringify(fingerprints, null, 4));
                            sock.write('30 /filemanager\r\n');
                        } else {
                            sock.write('20 text/gemini\r\n# Login failed\n\nyour username or password was incorrect.\n=> /filemanager/username Try again\r\n');
                        }
                    } else {
                        sock.write('11 Please enter your password\r\n');
                    }
                } else {
                    sock.write('51 Not Found\r\n');
                }
            } else {
                sock.write('30 /filemanager/username\r\n');
            }
        }
    } else {
        sock.write('60 Must use client certificate to log in\r\n');
    }
}

let uploading = {};

function upload(sock, path, query, data, containsRoute) {
    let fingerprint = sock.getPeerCertificate(true).fingerprint256;
    if (fingerprint) {
        let fingerprints = JSON.parse(fs.readFileSync('fingerprints.json').toString());
        if (fingerprints[fingerprint]) {
            let username = fingerprints[fingerprint];
            if(containsRoute){
                safepath = path.split(';')[0];
                var size;
                for(var d of path.split(';')){if(d.startsWith('size')){size=d.split('=')[1]}};
                uploading[username] = {
                    uploaded: data.slice(data.indexOf('\r\n')+2).length,
                    size
                }
                fs.writeFileSync('sites/'+username+safepath, data.slice(data.indexOf('\r\n')+2));
            }else{
                uploading[username].uploaded += data.length;
                fs.appendFileSync('sites/'+username+safepath, data);
            }
            if (uploading[username].uploaded >= uploading[username].size) {
                delete uploading[username];
                path = path.split(';')[0];
                sock.write('20 text/gemini\r\n');
                sock.write('# Success!\n\n=> gemini://'+query+'/filemanager Back');
                sock.end();
            }
        }
    }
}

function showFiles(username, path, sock, raw) {
    let dir = p.join('sites/'+username+'/',path).replace(/\\/g,"/");
    if(dir.endsWith('/')){
        let files = fs.readdirSync(dir,{ withFileTypes: true });
        let userpath = dir.replace('sites/'+username, "");
        sock.write('20 text/gemini\r\n');
        sock.write('# List of '+userpath+(userpath=="/"?"":"\n=> .. Back"));
        for(let file of files){
            sock.write("\n=> /filemanager"+userpath+file.name+(file.isDirectory() ? "/ 📁"+file.name+"/":" 📝"+file.name));
        }
        sock.write("\n\n=> /filemanager"+path+"~newfile New File\n=> /filemanager"+path+"~newdirectory New Directory"+(userpath=="/"?"":"\n=> /filemanager"+path+"~deldirectory Delete Directory"));
    }else{
        if (raw) {
            if (!fs.existsSync(dir)) return sock.write('20 text/plain\r\nNo file.\r\n');
            let file = fs.readFileSync(dir);
            if (file == "") return sock.write('20 text/plain\r\nEmpty File.\r\n');
            sock.write('20 text/plain\r\n');
            sock.write(file);
        } else {
            let userpath = dir.replace('sites/'+username, "");
            let blackdahlia = userpath.split("/");
            sock.write('20 text/gemini\r\n');
            sock.write("# "+userpath+"\n\n=> /filemanager"+blackdahlia.splice(0,blackdahlia.length-1).join('/')
                +"/ Back\n=> ?raw View Raw\n=> /filemanager"+path+"~movefile Move/Rename\n=> /filemanager"+path+"~delfile Delete\n=> titan://"
                +sock.servername+path+" Upload with Titan\r\n");
        }
    }
}

module.exports = { fileManager, upload }