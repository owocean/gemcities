const tls = require('tls');
const config = require('../config.json');
const getRoute = require('./parse');
const serveStatic = require('./static');
const serveUserPage = require('./userpage');
const titan = require('./titan');
const fs = require('fs');

const certs = {
    domain: {
        key: fs.readFileSync(config.domainKey),
        cert: fs.readFileSync(config.domainCert),
        requestCert: true,
        rejectUnauthorized: false
    },
    subdomain: {
        key: fs.readFileSync(config.subdomainKey),
        cert: fs.readFileSync(config.subdomainCert),
        requestCert: true,
        rejectUnauthorized: false
    }
}

const options = {
    requestCert: true,
    rejectUnauthorized: false,
    SNICallback: function (servername, cb) {
        if (config.customDomainsList.includes(servername)){
            let c = {
                key: fs.readFileSync('certs/'+config.customDomains[servername]+'-key.pem'),
                cert: fs.readFileSync('certs/'+config.customDomains[servername]+'-cert.pem'),
                rejectUnauthorized: false
            }
            let ctx = tls.createSecureContext(c);
            cb(null, ctx);
        } else {
            if (servername.split('.').length == 3) {
                let ctx = tls.createSecureContext(certs.domain);
                cb(null, ctx);
            } else {
                let ctx = tls.createSecureContext(certs.subdomain);
                cb(null, ctx);
            }
        }
    }
}

const server = tls.createServer(options, function (sock) {
    getRoute(sock, function (args) {
        if (args.homepage) {
            serveStatic(sock, args.path, args.query);
        } else if (args.titan) {
            titan.upload(sock, args.path, args.query, args.data, args.containsRoute);
        } else {
            serveUserPage(sock, args);
        }
    }, function () {
        if(sock.destroyed)return;
        sock.write('40 Did you set an identity?\r\n');
        sock.end();
    });
});

module.exports = server;