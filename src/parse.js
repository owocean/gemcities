const url = require('url');
const config = require('../config.json');

let uploaders = {};

function getRoute(sock, resolve, reject) {
    //return new Promise((resolve, reject) => {
        setTimeout(reject, 5000);
        let fingerprint = sock.getPeerCertificate(true).fingerprint256;
        sock.on('data', (data) => {
            if (fingerprint && uploaders[fingerprint]){
                resolve({
                    titan: true,
                    data
                });
            }else{
                const req = url.parse(data.toString().split('\r\n')[0]);
                const host = req.hostname.split('.');
                let path = req.pathname || "/";
                if (req.protocol == "gemini:"){
                    if (config.customDomainsList.includes(req.hostname)){
                        resolve({
                            user: config.customDomains[req.hostname],
                            path
                        });
                    } else {
                        if (host.length == 3) {
                            resolve({
                                homepage: true,
                                path,
                                query: req.query
                            });
                        }else{
                            resolve({
                                user: host[0],
                                path,
                                query: req.query
                            });
                        }
                    }
                }else if(req.protocol == "titan:"){
                    if (!fingerprint) reject();
                    if (host.length == 3) {
                        uploaders[fingerprint] = true;
                        resolve({
                            titan: true,
                            path,
                            query: req.hostname,
                            data,
                            containsRoute: true,
                        });
                    }
                }
            }
        });
        sock.on('error', (err) => {});
        sock.on('close', () => {
            if (fingerprint && uploaders[fingerprint]){
                uploaders[fingerprint] = false;
            }
        });
    //});
}

module.exports = getRoute;
