const fs = require('fs');
const mime = require('./mime');
const {VM} = require('vm2');

if (!fs.existsSync('sites/')) {
    fs.mkdirSync('sites/');
}

function serveUserPage(sock, args) {
    const dir = fs.readdirSync("sites/");
    if (dir.includes(args.user)) {
        if (!args.path) return sock.write('51 Not Found\r\n');
        if (args.path.endsWith("/")) {
            if (!fs.existsSync("sites/" + args.user + args.path)) {
                sock.write('51 Not Found\r\n');
                return sock.end();
            }
            const servedir = fs.readdirSync("sites/" + args.user + args.path);
            if (!servedir.includes('index.gmi')) {
                sock.write('20 text/gemini\r\n');
                sock.write('# Indexed files\n\n');
                servedir.forEach((file) => {
                    sock.write('=> ' + file + '\n');
                });
            } else {
                const contents = fs.readFileSync('sites/' + args.user + args.path + 'index.gmi');
                let payload = render(contents.toString(), args.user, args.query);
                if (payload.special) {
                    if (payload.input) {
                        sock.write('10 '+payload.input+'\r\n');
                    }
                    if (payload.redirect) {
                        sock.write('30 '+payload.redirect+'\r\n');
                    }
                } else {
                    sock.write('20 text/gemini\r\n');
                    sock.write(payload.text);
                }
            }
        } else if (fs.existsSync('sites/' + args.user + args.path)) {
            if (fs.lstatSync('sites/' + args.user + args.path).isDirectory()) {
                sock.write('31 '+args.path+'/\r\n');
            } else {
                const contents = fs.readFileSync('sites/' + args.user + args.path);
                const mimetype = mime(args.path);
                let payload = mimetype == "text/gemini" ? render(contents.toString(), args.user, args.query) : {text:contents};
                if (payload.special) {
                    if (payload.input) {
                        sock.write('10 '+payload.input+'\r\n');
                    }
                    if (payload.redirect) {
                        sock.write('30 '+payload.redirect+'\r\n');
                    }
                } else {
                    sock.write('20 ' + mimetype + '\r\n');
                    sock.write(payload.text);
                }
            }
        } else {
            sock.write('51 Not Found\r\n');
        }
    } else {
        sock.write('51 Not Found\r\n');
    }
    sock.end();
}

function render(text, user, query){
    return execute(text
        .replace(/{{GUESTBOOK}}/g, getGuestbook(user))
        .replace(/{{BLACK}}/g,  "\033[0;30m")
        .replace(/{{RED}}/g,    "\033[0;31m")
        .replace(/{{GREEN}}/g,  "\033[0;32m")
        .replace(/{{YELLOW}}/g, "\033[0;33m")
        .replace(/{{BLUE}}/g,   "\033[0;34m")
        .replace(/{{PURPLE}}/g, "\033[0;35m")
        .replace(/{{CYAN}}/g,   "\033[0;36m")
        .replace(/{{WHITE}}/g,  "\033[0;37m")
        .replace(/{{RESET}}/g,  "\033[0;0m"), user, query)
}

function getGuestbook(user) {
    let guestbooks = JSON.parse(fs.readFileSync('guestbooks.json').toString());
    if (guestbooks[user]) {
        let output = "";
        for (let entry of guestbooks[user].reverse()) {
            output += "> "+entry.text+"\n  - "+entry.user+"\n";
        }
        return output;
    } else {
        return "There are currently no messages.\n"
    }
}

function execute(text, user, query) {
    if (fs.readFileSync('hasJSperms.txt').toString().includes(user)) {
        const vm = new VM({
            timeout: 100,
            allowAsync: false,
            sandbox: {echo, redirect, input}
        });
        let lines = text.split('\n');
        let output = [];
        let jscode = "";
        let js = false;
        let redir = null;
        let inp = null;
        function echo(stuff) {
            output.push(stuff);
        }
        function redirect(path) {
            redir = path;
        }
        function input(question) {
            if (query) {
                return query;
            } else {
                inp = question
            }
        }
        for (let line of lines) {
            if (line.trim() == ";;") {
                js = !js;
                try {
                    vm.run(jscode);
                } catch (e) {
                    output.push(e);
                }
                jscode = "";
                continue;
            }
            if (js) {
                jscode += line+'\n';
            } else {
                output.push(line);
            }
        }
        let joined = {text: output.join('\n')};
        if (redir != null) {
            joined['redirect'] = redir;
        }
        if (inp != null) {
            joined['input'] = inp;
        }
        if (inp != null || redir != null) {
            joined['special'] = true;
        }
        return joined;
    } else {
        return { text };
    }
}

module.exports = serveUserPage;
