const account = require('./account');

function signUp(sock, isPasswd, query, path) {
    if (!isPasswd) {
        if (query) {
            if (query.length > 30) return sock.write("10 Your username is too long! 30 character limit.\r\n");
            let check = account.checkIllegalChars(query);
            if (check.bad) {
                sock.write(`10 `+check.reason+`\r\n`);
            } else {
                sock.write(`30 /signup/${query}\r\n`);
            }
        } else {
            sock.write(`10 Please enter your desired username\r\n`);
        }
    } else {
        if (query) {
            let username = path.split('/')[2];
            let password = decodeURIComponent(query);
            account.create(username, password);
            sock.write(`30 /welcome.gmi\r\n`);
        } else {
            sock.write(`11 Enter your desired password\r\n`);
        }
    }
}

module.exports = signUp;