const bcrypt = require('bcryptjs');
const fs = require('fs');
const yaml = require('js-yaml');

function login(username, password) {
    let config = yaml.load(fs.readFileSync('config/config.yaml').toString());
    if (!config.users[username]) return false;
    if (bcrypt.compareSync(password, config.users[username].password)) {
        return true;
    } else {
        return false;
    }
}

function hash(password) {
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);
    return hash;
}

function create(username, password) {
    username = username.toLowerCase();
    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);
    let config = fs.readFileSync('config/config.yaml').toString();
    config += `  ${username}:
    password: "${hash}"
    subdir: "/${username}"\n`;
    if (fs.existsSync('sites/' + username)) return;
    fs.writeFileSync('config/config.yaml', config);
    fs.mkdirSync('sites/' + username);
    fs.writeFileSync('sites/'+username+'/index.gmi', '# Hello World!');
}

function checkIllegalChars(name) {
    let chars = "./\\\n\r%?&:\"' `";
    for (let char of name.split("")) {
        if (chars.includes(char)) {
            return {
                bad: true,
                reason: "Illegal character \"" + char + "\""
            };
        }
    }
    return {
        bad: false
    };
}

module.exports = { login, create, checkIllegalChars, hash }
