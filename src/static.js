const config = require('../config.json');
const fs = require('fs');
const p = require('path');
const mime = require('./mime');
const signUp = require('./signup');
const titan = require('./titan');
const account = require('./account');

function serveStatic(sock, path, query) {
    if (!path) return;
    const dir = fs.readdirSync(config.staticFilesDir);
    if (path == "/" && dir.includes('index.gmi')) {
        const contents = fs.readFileSync(p.join(config.staticFilesDir, 'index.gmi'));
        sock.write('20 text/gemini\r\n');
        sock.write(contents);
    } else if (path == "/signup" || path.startsWith("/signup/")) {
        signUp(sock, path.startsWith("/signup/") ? true : false, query, path);
    } else if (path.startsWith("/guestbook/") && path != "/guestbook/") {
        guestbook(sock,query,path);
    } else if (path.startsWith("/filemanager")) {
        titan.fileManager(sock,query,path);
    } else if (path == "/passwd") {
        if (query) {
            sock.write('20 text/plain\r\n');
            sock.write(account.hash(query)+'\r\n');
        }else{
            sock.write('11 Please enter your desired password\r\n');
        }
    } else if (dir.includes(path.substr(1))) {
        const contents = fs.readFileSync(p.join(config.staticFilesDir, path));
        const mimetype = mime(path);
        sock.write('20 '+mimetype+'\r\n');
        sock.write(contents);
    } else if (path == "/" && !dir.includes('index.gmi')) {
        sock.write('20 text/gemini\r\n');
        sock.write('# Indexed files\n\n');
        dir.forEach((file) => {
            sock.write('=> '+file+'\n');
        });
    } else {
        sock.write('51 Not Found\r\n');
    }
    sock.end();
}

module.exports = serveStatic;

let annoyances = [];

function guestbook(sock,query,path) {
    if (annoyances[sock.remoteAddress]) return sock.write('44 60\r\n');
    if (query) {
        let args = path.split('/');
        let username = args[2];
        if (args[3] == undefined || args[3] == "") {
            sock.write('30 /guestbook/'+username+'/'+query+'\r\n');
        } else {
            let guestbooks = JSON.parse(fs.readFileSync('guestbooks.json').toString());
            if (!guestbooks[username]) {
                guestbooks[username] = [];
            }
            guestbooks[username].push({
                text: decodeURIComponent(query),
                user: decodeURIComponent(args[3])
            });
            fs.writeFileSync('guestbooks.json', JSON.stringify(guestbooks));
            sock.write('30 gemini://'+username+'.cities.yesterweb.org/\r\n');
            annoyances[sock.remoteAddress] = true;
            setTimeout(function() {
                delete annoyances[sock.remoteAddress]
            },60000)
        }
    } else {
        let args = path.split('/');
        if (args[3] == undefined || args[3] == "") {
            sock.write('10 What\'s your name?\r\n');
        } else {
            sock.write('10 Go ahead and write your message! (Try to keep it short) Remember to be kind :)\r\n');
        }
    }
}