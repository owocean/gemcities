const server = require('./src/sock');
require('./src/http');
const config = require('./config.json');

server.listen(config.listenPort || 1965);
console.log("Gemini listening on port " + (config.listenPort || 1965));